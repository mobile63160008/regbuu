import 'package:flutter/material.dart';

class schedule extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    return Scaffold(
        body: ListView(children: <Widget>[
          Center(
              child: Text(
                '',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              )),
          DataTable(columnSpacing: 10.0,
            columns: [
              DataColumn(label: Text(
                  'Day',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              )),
              DataColumn(label: Text(
                  'Time',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)
              )),
              DataColumn(label: Text(
                  'Subject',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)
              )),
              DataColumn(label: Text(
                  'Room',
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)
              )),
            ],
            rows: [
              DataRow(cells: [
                DataCell(Text('Monday')),
                DataCell(Text('10:00-12:00')),
                DataCell(Text('Software Testing')),
                DataCell(Text('3M210'))
              ]),
              DataRow(cells: [
                DataCell(Text('')),
                DataCell(Text('13:00-15:00')),
                DataCell(Text('Object-Oriented Analysis and Design')),
                DataCell(Text('3M210'))
              ]),
              DataRow(cells: [
                DataCell(Text('')),
                DataCell(Text('17:00-19:00')),
                DataCell(Text('Web Programming')),
                DataCell(Text('3M210')),
              ]),
              DataRow(cells: [
                DataCell(Text('Tuesday')),
                DataCell(Text('10:00-12:00')),
                DataCell(Text('Multimedia Programming')),
                DataCell(Text('4C02')),
              ]),DataRow(cells: [
                DataCell(Text('')),
                DataCell(Text('13:00-15:00')),
                DataCell(Text('Software Testing')),
                DataCell(Text('3C03')),
              ]),DataRow(cells: [
                DataCell(Text('')),
                DataCell(Text('17:00-19:00')),
                DataCell(Text('Object-Oriented Analysis and Design')),
                DataCell(Text('4C01')),
              ]),
              DataRow(cells: [
                DataCell(Text('Wednesday')),
                DataCell(Text('10:00-12:00')),
                DataCell(Text('Mobile Application')),
                DataCell(Text('4C02')),
              ]),DataRow(cells: [
                DataCell(Text('')),
                DataCell(Text('13-00:15:00')),
                DataCell(Text('Multimedia Programming')),
                DataCell(Text('4C01')),
              ]),DataRow(cells: [
                DataCell(Text('')),
                DataCell(Text('15:00-17:00')),
                DataCell(Text('Web Programming')),
                DataCell(Text('3C01')),
              ]),DataRow(cells: [
                DataCell(Text('Thursday')),
                DataCell(Text('13:00-16:00')),
                DataCell(Text('Justice Administration and Laws in Everyday Life')),
                DataCell(Text('4C02')),
              ]),DataRow(cells: [
                DataCell(Text('Friday')),
                DataCell(Text('9:00-12:00')),
                DataCell(Text('Introduction to Natural Language Processing')),
                DataCell(Text('5T05')),
              ]),DataRow(cells: [
                DataCell(Text('')),
                DataCell(Text('13:00-15:00')),
                DataCell(Text('Mobile Application')),
                DataCell(Text('4C02')),
              ]),
            ],
          ),
        ])
    );
  }
}