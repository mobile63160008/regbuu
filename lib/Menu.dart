import 'package:column_widget_example/news.dart';
import 'package:column_widget_example/resume.dart';
import 'package:column_widget_example/login_page.dart';
import 'package:column_widget_example/schedule.dart';
import 'package:flutter/material.dart';

class Menu extends StatefulWidget {
  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<Menu> {
  Widget _currentPage = news();

  void _changePage(Widget page) {
    setState(() {
      _currentPage = page;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
          backgroundColor: Colors.grey.shade300,
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              UserAccountsDrawerHeader(
                accountName: Text("Nontalee"),
                accountEmail: Text("63160008@go.buu.ac.th"),
                currentAccountPicture: CircleAvatar(
                  child: ClipOval(
                    child: Image.network(
                      "https://64.media.tumblr.com/06998bef1f4b2c5430da1713f9d29d43/29e23c84347492ea-1c/s540x810/ee374ec77307d116c94f44022f1f45b401da0e27.jpg",
                      width: 90,
                      height: 90,
                    ),
                  ),
                ),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage('https://staticg.sportskeeda.com/editor/2023/01/b66ee-16737224349984-1920.jpg',
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              ListTile(
                leading: Icon(Icons.newspaper, color: Colors.black),
                title: Text("News"),
                onTap: () {
                  _changePage(news());
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(Icons.person, color: Colors.black),
                title: Text("Resume"),
                onTap: () {
                  _changePage(resume());
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(Icons.view_comfortable, color: Colors.black),
                title: Text("Schedule"),
                onTap: () {
                  _changePage(schedule());
                  Navigator.pop(context);
                },
              ),

              ListTile(
                leading: Icon(Icons.logout, color: Colors.black),
                title: Text("Logout"),
                onTap: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => LoginPage()));
                },
              ),
            ],
          )),
      appBar: AppBar(
        title: Text("REG BUU"),
        actions: [
          IconButton(
              icon: Icon(Icons.logout,color:Colors.black),
              onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) {
                      return LoginPage();
                    }));
              })
        ],
      ),
      body: _currentPage,
    );
  }
}