import 'package:column_widget_example/Menu.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register Burapha"),
      ),
      body: Form(

        child: Center(
          child: Column(
            children: [
              const Spacer(flex: 6),
              Image.network(
                'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Buu-logo11.png/1024px-Buu-logo11.png',height: 200,
              ),

              // const SizedBox(height: .0),
              const SizedBox(width:200,child:TextField(decoration: InputDecoration(labelText: 'Username'))),
              const SizedBox(width:200,child:TextField(decoration: InputDecoration(labelText: 'Password'))),
              const Spacer(flex: 5),
              ElevatedButton(onPressed: () {
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context){
                      return Menu();
                    }));
              }, child: const Text('Login')),
              const Spacer(flex: 8),
            ],
          ),
        ),
      ),
    );
  }
}


