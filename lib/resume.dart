import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class resume extends StatefulWidget {
  @override
  _MainMenuState createState() => _MainMenuState();
}
class _MainMenuState extends State<resume>{
  final double coverHeight = 200;
  final double profileHeight = 180;
  @override
  Widget build(BuildContext context){
    final top = coverHeight - profileHeight /2;
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          buildTop(),
          buildContent(),
        ],
      ),
    );
  }
  Widget buildContent() => Column(
    children: [
      const SizedBox(height: 8),
      Text('นนทลี สามารถ',style: TextStyle(fontSize: 28,fontWeight: FontWeight.bold),
      ),

      const SizedBox(height: 8),
      Text('วิทยาการคอมพิวเตอร์',style: TextStyle(fontSize: 20,fontWeight:FontWeight.normal),
      ),
      const SizedBox(height: 40),
      Text('ประวัตินิสิต',style: TextStyle(fontSize: 20,fontWeight:FontWeight.bold),
      ),const SizedBox(height: 8),
      Text('รหัสประจำตัว : 63160008',style: TextStyle(fontSize: 20,fontWeight:FontWeight.normal),
      ),
      const SizedBox(height: 8),
      Text('ชื่อ : นางสาวนนทลี สามารถ',style: TextStyle(fontSize: 20,fontWeight:FontWeight.normal),
      ),
      const SizedBox(height: 8),
      Text('อายุ 21 ปี เพศหญิง',style: TextStyle(fontSize: 20,fontWeight:FontWeight.normal),
      ),
      const SizedBox(height: 8),
      Text('คณะวิทยาการสารสนเทศ',style: TextStyle(fontSize: 20,fontWeight:FontWeight.normal),
      ),
      const SizedBox(height: 8),
      Text('มหาวิทยาลัยบูรพา',style: TextStyle(fontSize: 20,fontWeight:FontWeight.normal),
      ),
      const SizedBox(height: 8),
      Text('วันเกิด : 4/07/2544',style: TextStyle(fontSize: 20,fontWeight:FontWeight.normal),
      ),const SizedBox(height: 8),
      Text('GPA : 3.25',style: TextStyle(fontSize: 20,fontWeight:FontWeight.normal),
      ),

    ],
  );
  Widget buildTop(){
    final bottom = profileHeight /2;
    final top = coverHeight - profileHeight /2;
    return Stack(
      clipBehavior: Clip.none,
      alignment: Alignment.center,
      children : [
        Container(
          margin : EdgeInsets.only(bottom: bottom),
          child : buildCoverImage() ,
        ),
        Positioned(
          top: top,
          child: buildProfileImage(),)
      ],
    );
  }
  Widget buildCoverImage() => Container(
    color: Colors.black,
    child: Image.network('https://media.discordapp.net/attachments/642334183112310796/1070298115757834320/dbe07434-7f35-4f0d-bf3a-eac31d5758eb.png?width=1409&height=683'),
  );
  Widget buildProfileImage() => CircleAvatar(
    radius: profileHeight/2,
    backgroundColor: Colors.black,
    backgroundImage: NetworkImage(
      'https://media.discordapp.net/attachments/642334183112310796/1070299573051994152/2a051ff45aadc6d32f5c48ca22b9fb03.jpg'
    ),
  );
}
  // Widget build(BuildContext context){
  //   return SingleChildScrollView(
  //       child:Container(
  //         padding: EdgeInsets.all(20),
  //         child: Column(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           children: [
  //             Card(
  //               child: Column(
  //                 children: <Widget>[
  //                   Stack(
  //                     children: <Widget>[
  //                       Image.network(
  //                         'https://media.discordapp.net/attachments/642334183112310796/1070279616863928390/tumblr_0ebedaf775393a9d2c788ff5e22daa5d_9b9e99e2_1280_cleanup.png',
  //                         fit: BoxFit.cover,
  //                       ),
  //                       Container(
  //                         alignment: Alignment.topLeft,
  //                         decoration: BoxDecoration(
  //                           // gradient: LinearGradient(
  //                           //   begin: Alignment.bottomCenter,
  //                           //   end: Alignment.topCenter,
  //                           //   colors: [
  //                           //     Colors.transparent.withOpacity(0.7),
  //                           //     Colors.transparent.withOpacity(0.0),
  //                           //   ],
  //                           // ),
  //                         ),
  //                         child: Padding(
  //                           padding: EdgeInsets.all(8.0),
  //                           child: Column(
  //                             crossAxisAlignment: CrossAxisAlignment.start,
  //                             mainAxisAlignment: MainAxisAlignment.end,
  //                             children: <Widget>[
  //
  //                               Text(
  //                                 'ประวัตินิสิต',
  //                                 style: TextStyle(
  //                                   color: Colors.black,
  //                                   fontSize: 35,
  //                                 ),
  //                               ),
  //                               SizedBox(
  //                                 height: 8,
  //                               ),
  //                               Text(
  //                                 'ของนางสาวนนทลี สามารถ',
  //                                 style: TextStyle(
  //                                   color: Colors.black45,
  //                                   fontSize: 15,
  //                                 ),
  //                               ),
  //                             ],
  //                           ),
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ],
  //               ),
  //             ),
  //             Card(
  //               child: Column(
  //                 children: <Widget>[
  //                   Stack(
  //                     children: <Widget>[
  //                       Image.network(
  //                         'https://media.discordapp.net/attachments/642334183112310796/1070271485710712894/white-concrete-wall.jpg?width=1023&height=683',
  //                         fit: BoxFit.cover,
  //                       ),
  //                       Container(
  //                         height: 200,
  //                         width: 500,
  //                         decoration: BoxDecoration(
  //                           // gradient: LinearGradient(
  //                           //   begin: Alignment.bottomCenter,
  //                           //   end: Alignment.topCenter,
  //                           //   colors: [
  //                           //     Colors.transparent.withOpacity(0.7),
  //                           //     Colors.transparent.withOpacity(0.0),
  //                           //   ],
  //                           // ),
  //                         ),
  //                         child: Padding(
  //                           padding: EdgeInsets.all(5.0),
  //                           child: Column(
  //                             crossAxisAlignment: CrossAxisAlignment.start,
  //                             mainAxisAlignment: MainAxisAlignment.end,
  //                             children: <Widget>[
  //                               Text(
  //                                 'รหัสประจำตัว : 63160008 \n'
  //   'ชื่อ : นางสาวนนทลี  สามารถ \n'
  //                                 'คณะวิทยาการสารสนเทศ \n'
  //                                     'มหาวิทยาลัยบูรพา วิทยาเขตบางแสน \n'
  //                                 'ระดับการศึกษา : ปริญญาตรี \n'
  //                                 'อ. ที่ปรึกษา : อาจารย์ภูสิต กุลเกษม,\n'
  //                                     'ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน ',
  //
  //                                 style: TextStyle(
  //                                   color: Colors.black,
  //                                   fontSize: 18,
  //                                 ),
  //                               ),
  //                               SizedBox(
  //                                 height: 8,
  //                               ),
  //                             ],
  //                           ),
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ],
  //               ),
  //             ),
  //             Card(
  //               child: Column(
  //                 children: <Widget>[
  //                   Stack(
  //                     children: <Widget>[
  //                       Image.network(
  //                         'https://media.discordapp.net/attachments/642334183112310796/1070280813775683605/saas.jpg',
  //                         fit: BoxFit.cover,
  //                       ),
  //                       Container(
  //                         height: 200,
  //                         width: 500,
  //                         decoration: BoxDecoration(
  //                           // gradient: LinearGradient(
  //                           //   begin: Alignment.bottomCenter,
  //                           //   end: Alignment.topCenter,
  //                           //   colors: [
  //                           //     Colors.transparent.withOpacity(0.7),
  //                           //     Colors.transparent.withOpacity(0.0),
  //                           //   ],
  //                           // ),
  //                         ),
  //                         child: Padding(
  //                           padding: EdgeInsets.all(5.0),
  //                           child: Column(
  //                             crossAxisAlignment: CrossAxisAlignment.start,
  //                             mainAxisAlignment: MainAxisAlignment.end,
  //                             children: <Widget>[
  //                               Text(
  //                                 'STUDENT ID : 63160008 \n'
  //                                     'MISS Nontalee Samart \n'
  //                                     'INFORMATICS \n'
  //                                     'BIRTHDAY : 4 July 2001  \n'
  //                                     'GPA : 3.25 \n'
  //                                 'TEL : 085-3861336 \n'
  //                                 'INSTAGRAM : bammeisterx',
  //
  //                                 style: TextStyle(
  //                                   color: Colors.black,
  //                                   fontSize: 18,
  //                                 ),
  //                               ),
  //                               SizedBox(
  //                                 height: 8,
  //                               ),
  //                             ],
  //                           ),
  //                         ),
  //                       ),
  //                     ],
  //                   ),
  //                 ],
  //               ),
  //             ),
  //           ],
  //         ),
  //       )
  //   );
  // }
